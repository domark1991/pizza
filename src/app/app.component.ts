import { Component } from '@angular/core';
import { FilterModel } from './models/filter.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  filterValue: FilterModel;

  filterChanged(event: any) {
    console.log('FilterChanged');
    this.filterValue = event;
  }
}
