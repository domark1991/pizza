import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-pizza-detail',
  templateUrl: './pizza-detail.component.html',
})

export class PizzaDetails {
  @Input() pizza: any;
}
