import { Component, AfterViewInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { PizzaService } from '../service/pizza.service';
import { FilterModel } from '../models/filter.model';

@Component({
  selector: 'app-pizza-list',
  templateUrl: './pizza-list.component.html',
})

export class PizzaList implements AfterViewInit, OnChanges {
  private pizzalist: any[];
  @Input() filterValue: FilterModel;

  constructor(private prizzaService: PizzaService) { }

  ngAfterViewInit() {
    this.prizzaService.getDataFromApi().subscribe(data => {
      this.pizzalist = data;
    })
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes)
    if (changes['filterValue']) {
      this.prizzaService.getDataFromApi(this.filterValue).subscribe(data => {
        this.pizzalist = data;
      })
    }
  }
}
