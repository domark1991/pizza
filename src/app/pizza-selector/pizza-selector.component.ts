import { Component, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { PizzaService } from '../service/pizza.service';
import { FilterModel } from '../models/filter.model';

@Component({
  selector: 'app-pizza-selector',
  templateUrl: './pizza-selector.component.html',
  styleUrls: ['./pizza-selector.component.scss']
})

export class PizzaSelector implements AfterViewInit {
  price_range: any;
  pizza_type: any;
  pizza_base: any;

  min_price: number;
  max_price: number;
  pizza_base_list: any[];
  pizza_type_list: any[];

  currentFilter: FilterModel;

  @Output() filterValue: EventEmitter<FilterModel> = new EventEmitter();

  constructor(private pizzaService: PizzaService) {

  }

  ngAfterViewInit() {
    this.pizzaService.getFilterValues().subscribe(data => {
      this.max_price = data.max_price;
      this.min_price = data.min_price;

      this.pizza_type_list = data.pizza_type;
      this.pizza_base_list = data.pizza_base;
    });

    this.currentFilter = new FilterModel();
  }


  onSelectPizzaBase(value: any) {
    this.pizza_base = value;
    this.currentFilter.pizza_base = value;
    this.publishFilter();
  }

  onSelectPizzaType(value: any) {
    this.pizza_type = value;
    this.currentFilter.pizza_type = value;
    this.publishFilter();
  }

  onPriceRangeChanged(value: any) {
    this.currentFilter.max_price = this.price_range
    this.publishFilter();
  }

  isVeg(value: boolean) {
    this.currentFilter.isVeg = value;
    this.publishFilter();
  }

  publishFilter() {
    let newfilter = new FilterModel();
    newfilter.copy(this.currentFilter);
    this.filterValue.emit(newfilter);
  }
}
