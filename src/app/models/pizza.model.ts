export class PizzaModel {
    pizza_title: string;
    pizza_base: string;
    pizza_type: string;
    pizza_veg: boolean;
    pizza_price: number;
    image_path: string;
}