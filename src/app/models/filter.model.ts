export class FilterModel {
    isVeg = false;
    max_price: number = 0;
    pizza_base: string;
    pizza_type: string;

    constructor() {
        this.isVeg = false;
        this.max_price = 0;
        this.pizza_base = '';
        this.pizza_type = '';
    }

    copy(filterModel: FilterModel) {
        this.isVeg = filterModel.isVeg;
        this.max_price = filterModel.max_price;
        this.pizza_base = filterModel.pizza_base;
        this.pizza_type = filterModel.pizza_type;
    }

}