import { Injectable } from '@angular/core';
import { FilterModel } from '../models/filter.model';
import { PizzaModel } from '../models/pizza.model';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs';
import { timer } from 'rxjs/observable/timer';
import { of } from 'rxjs/observable/of';
import { delay } from 'rxjs/operators';

@Injectable()
export class PizzaService {

    private all_pizza: any[];
    private pizza_observable: Observable<any>;

    private filterValues: any;
    private filterObservable: Observable<any>;

    constructor(private http: HttpClient) {

    }

    getFilterValues() {
        if (this.filterValues)
            return Observable.of(this.filterValues);
        else if (this.filterObservable) { return this.filterObservable }
        else {
            this.filterObservable = this.http.get<any>('./assets/filter-values.json').map(data => {
                this.filterValues = data;
                this.filterObservable = null;
                return this.filterValues;
            });
            return this.filterObservable;
        }
    }

    getDataFromApi(pizzaFilter: FilterModel = null) {
        if (this.all_pizza)
            return Observable.of(this.filter(this.all_pizza, pizzaFilter));
        else if (this.pizza_observable) { return this.pizza_observable }
        else {
            this.pizza_observable = this.http.get('./assets/data.json').map(data => {
                if (data) {
                    this.all_pizza = data['pizza-data'];
                    this.pizza_observable = null;
                    return this.filter(this.all_pizza, pizzaFilter);
                }
            });
            return this.pizza_observable;
        }
    }

    private filter(all_pizza: any[], pizzaFilter: FilterModel) {
        if (pizzaFilter) {
            return all_pizza.filter(item => {
                return (item.pizza_veg === pizzaFilter.isVeg) && (pizzaFilter.pizza_base === '' || item.pizza_base.value === pizzaFilter.pizza_base) && (pizzaFilter.max_price === 0 || item.pizza_price <= pizzaFilter.max_price) && (pizzaFilter.pizza_type === '' || item.pizza_type.value === pizzaFilter.pizza_type);
            })
        } else {
            return all_pizza;
        }
    }

}